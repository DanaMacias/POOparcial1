package logica;

public class Mujer extends TMB {
	public Mujer(String nombre, double pesoK, double altura, double edad) {
		super(nombre, pesoK, altura, edad);
	}
	
	public Mujer() {
		super();
	}

	@Override
	public double calcularTMB() {
		return 447.593 + (9.247 * this.pesoK) + (3.098 * this.altura)-(4.33 * this.edad);
	}

	@Override
	public String toString() {
		return super.toString() + "\n La tasa metabolica basal es: " + calcularTMB();
	}
}
