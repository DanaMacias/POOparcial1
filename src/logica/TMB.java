package logica;

public abstract class TMB {
	protected String nombre;
	protected double pesoK, altura, edad;
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public double getPesoK() {
		return pesoK;
	}

	public void setPesoK(double pesoK) {
		this.pesoK = pesoK;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	public double getEdad() {
		return edad;
	}

	public void setEdad(double edad) {
		this.edad = edad;
	}

	public TMB(String nombre, double pesoK, double altura, double edad) {
		this.nombre= nombre;
		this.pesoK = pesoK;
		this.altura = altura;
		this.edad = edad;
	}
	
	public TMB() {
		this.nombre= "";
		this.pesoK = 0;
		this.altura = 0;
		this.edad = 0;
	}
	
	
	
	@Override
	public String toString() {
		return "\n Nombre: " + nombre + "\n Peso: " + pesoK + "\n Altura: " + altura + "\n Edad: " + edad;
	}

	public abstract double calcularTMB();
	
}
