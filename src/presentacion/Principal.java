package presentacion;

import java.util.Scanner;

import logica.Hombre;
import logica.Mujer;

public class Principal {
	
	public Principal() {
		this.menu();
	}
	
	public static void main(String[] args) {
		new Principal();
		
	}

	private void menu() {
		Scanner sc = new Scanner(System.in);
		int op = 0;
		do {
			try {
				System.out.println("\n0. Salir \n1. Mujer \n2. Hombre\n Opcion: ");
				op= sc.nextInt();
				if(op == 1) {
					this.crearMujer(sc);				
				}else if(op == 2) {
					this.crearHombre(sc);
				}
			} catch (Exception e) {
				e.getMessage();
			}
		}while(op != 0);

		sc.close();

	}
	private void crearMujer(Scanner sc) throws Exception {
		System.out.println("Nombre: ");
		String nom= sc.next();
		System.out.println("Peso en Kilos: ");
		double pes= sc.nextDouble();
		System.out.println("Altura en cm: ");
		double alt= sc.nextDouble();
		System.out.println("Edad en años: ");
		double edad= sc.nextDouble();
		Mujer m = new Mujer(nom,pes,alt,edad);
		System.out.println(m.toString());
	}

	private void crearHombre(Scanner sc) throws Exception {
		System.out.println("Nombre: ");
		String nom= sc.next();
		System.out.println("Peso en Kilos: ");
		double pes= sc.nextDouble();
		System.out.println("Altura en cm: ");
		double alt= sc.nextDouble();
		System.out.println("Edad en años: ");
		double edad= sc.nextDouble();
		Hombre h = new Hombre(nom,pes,alt,edad);
		System.out.println(h.toString());
	}


}
